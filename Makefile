NULL :=
GAMES := \
	 amoebax \
	 bbb \
	 defeat-me \
	 escape-room \
	 absurdchain \
	 forca-magica-intergalactica \
	 hirsute \
	 kuudou \
	 ml2048rl \
	 mlrl \
	 monochromeworlds \
	 once-upon-a-time \
	 planet-music \
	 prison-break \
	 sisyphus \
	 streamlining \
	 toudaimori \
	 tramposillu \
	 umatantei \
	 undarodo \
	 worldsincards \
	 xenorogue \
	 $(NULL)
XML_GAMES := $(patsubst %,%/index.xml,$(GAMES))
HTML_GAMES := $(patsubst %.xml,%.html,$(XML_GAMES))
BLOG_ENTRIES := $(shell find blog -mindepth 1 -maxdepth 1 -type d -printf '%f\n' | sort -r)
XML_ENTRIES := $(patsubst %,blog/%/index.xml,$(BLOG_ENTRIES))
HTML_ENTRIES := $(patsubst %.xml,%.html,$(XML_ENTRIES))

HOME_XSL := index.xsl
GAME_XSL := game.xsl
POST_XSL := blog/post.xsl
INDEX_XSL := blog/index.xsl
COMMON_XSL := common.xsl

HOME_CSS := home.css
GAME_CSS := game.css
BLOG_CSS := blog/blog.css

DOCROOT = /srv/www/htdocs/www.peritasoft.com

all: blog/index.html $(HTML_GAMES) $(HOME_CSS) $(BLOG_CSS) $(GAME_CSS) index.html

.DELETE_ON_ERROR:

$(HOME_CSS): css/home.css css/reset.css css/base.css
	m4 -I css $< > $@

$(BLOG_CSS): css/blog.css css/reset.css css/base.css
	m4 -I css $< > $@

$(GAME_CSS): css/game.css css/reset.css css/base.css
	m4 -I css $< > $@

index.html: index.xml $(HOME_XSL) $(XML_GAMES) $(COMMON_XSL)
	xsltproc --output $@ $(HOME_XSL) $<

blog/index.html: blog/index.xml $(INDEX_XSL) $(COMMON_XSL)
	xsltproc --output $@ $(INDEX_XSL) $<

.INTERMEDIATE: blog/index.xml

blog/index.xml: $(HTML_ENTRIES)
	echo '<posts>' > $@
	for f in $(BLOG_ENTRIES); do \
		printf '<post src="%s/index.xml" href="%s/index.html"/>\n' $$f $$f >> $@; \
	done
	echo '</posts>' >> $@

blog/%/index.html: blog/%/index.xml $(POST_XSL) $(COMMON_XSL)
	xsltproc --output $@ $(POST_XSL) $<
	sed -i -e 's#</source>##g' $@

%/index.html: %/index.xml $(GAME_XSL) $(COMMON_XSL)
	xsltproc --output $@ $(GAME_XSL) $<

install: all
	install -d $(DESTDIR)$(DOCROOT)
	install -m 644 \
		favicon.ico \
		$(GAME_CSS) \
		game.js \
		$(HOME_CSS) \
		home.js \
		index.html \
		oculta.png \
		perita.svg \
		privacy.html \
		masonry.pkgd.min.js \
		$(DESTDIR)$(DOCROOT)
	install -d $(DESTDIR)$(DOCROOT)/css
	install -m 644 css/*.css $(DESTDIR)$(DOCROOT)/css
	install -d $(DESTDIR)$(DOCROOT)/blog
	install -d $(DESTDIR)$(DOCROOT)/store-badges
	install -m 644 store-badges/* $(DESTDIR)$(DOCROOT)/store-badges
	install -m 664 \
		blog/cc-by-sa-4.0-88x31.png \
		blog/index.html \
		$(BLOG_CSS) \
		$(DESTDIR)$(DOCROOT)/blog
	for f in $(BLOG_ENTRIES); do \
		install -d $(DESTDIR)$(DOCROOT)/blog/$$f ; \
		find blog/$$f -type f \! -name index.xml -print0 | xargs -0 install -m 644 -t $(DESTDIR)$(DOCROOT)/blog/$$f ; \
	done
	for g in $(GAMES); do \
		install -d $(DESTDIR)$(DOCROOT)/$$g ; \
		find $$g -type f \! -name index.xml -print0 | xargs -0 install -m 644 -t $(DESTDIR)$(DOCROOT)/$$g ; \
	done

clean:
	$(RM) $(HTML_ENTRIES) $(HTML_GAMES) index.html home.css $(BLOG_CSS) blog/index.xml blog/index.html

watch:
	while true; do $(MAKE); inotifywait --recursive --event modify .; done

.PHONY: all install clean watch
