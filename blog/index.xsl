<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:date="http://exslt.org/dates-and-times"
	extension-element-prefixes="date"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../common.xsl" />

	<xsl:variable name="base" select="'..'"/>

	<xsl:output
		method="html"
		version="1.0"
		encoding="UTF-8"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="about:legacy-compat"
		/>

	<xsl:template match="/">
		<html lang="en">
			<head>
				<xsl:call-template name="meta">
					<xsl:with-param name="description" select="'List of all the blog posts i have written while developing games.'"/>
				</xsl:call-template>
				<title>Blog post index by date — Perita</title>
				<link rel="stylesheet" type="text/css" media="screen" href="blog.css" />
			</head>
			<body>
				<xsl:call-template name="header">
					<xsl:with-param name="page" select="'blog'"/>
				</xsl:call-template>
				<article class="h-feed">
					<header>
						<h2 class="p-name">Post Index by Date</h2>
					</header>
					<xsl:apply-templates />
					<footer>
						<p>Last updated on <xsl:call-template name="date">
								<xsl:with-param name="datetime" select="date:date-time()"/>
						</xsl:call-template>.</p>
					</footer>
				</article>
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="posts">
		<ol id="posts">
			<xsl:apply-templates/>
		</ol>
	</xsl:template>

	<xsl:template match="post">
		<li class="h-entry">
			<xsl:apply-templates select="document(@src)/post/info">
				<xsl:with-param name="href" select="@href"/>
			</xsl:apply-templates>
		</li>
	</xsl:template>

	<xsl:template match="info">
		<xsl:param name="href"/>
		<p>
			<xsl:apply-templates select="title">
				<xsl:with-param name="href" select="$href"/>
			</xsl:apply-templates>
			<xsl:text>, </xsl:text>
			<xsl:apply-templates select="pubdate"/>
		</p>
		<xsl:apply-templates select="abstract"/>
	</xsl:template>

	<xsl:template match="title">
		<xsl:param name="href"/>
		<a class="p-name u-url" href="{$href}">
			<xsl:apply-templates/>
		</a>
	</xsl:template>

	<xsl:template match="abstract">
		<p class="p-summary"><xsl:apply-templates/></p>
	</xsl:template>

	<xsl:template match="pubdate">
		<xsl:call-template name="date">
				<xsl:with-param name="datetime" select="."/>
				<xsl:with-param name="class" select="'dt-published'"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:apply-imports/>
	</xsl:template>
</xsl:stylesheet>
