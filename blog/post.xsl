<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:date="http://exslt.org/dates-and-times"
	extension-element-prefixes="date"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../common.xsl" />

	<xsl:variable name="base" select="'../..'"/>

	<xsl:output
		method="html"
		version="1.0"
		encoding="UTF-8"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="about:legacy-compat"
		/>


	<xsl:template match="post">
		<html lang="en">
			<head>
				<xsl:call-template name="meta">
					<xsl:with-param name="description" select="info/abstract"/>
				</xsl:call-template>
				<title><xsl:apply-templates select="info/title" mode="text"/> — Perita</title>
				<link rel="stylesheet" type="text/css" media="screen" href="../blog.css" />
			</head>
			<body>
				<xsl:call-template name="header">
					<xsl:with-param name="page" select="'post'"/>
				</xsl:call-template>
				<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" class="h-entry">
					<xsl:apply-templates select="info"/>
					<div itemprop="articleBody" class="e-content">
						<xsl:apply-templates select="*[not(self::info)]"/>
					</div>
					<footer>
						<p><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" src="../cc-by-sa-4.0-88x31.png" width="88" height="31"/></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>
					</footer>
				</article>
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="info">
		<header>
			<xsl:apply-templates select="title"/>
			<xsl:apply-templates select="pubdate"/>
			<a href="/" rel="author" class="u-author" aria-hidden="true"></a>
		</header>
	</xsl:template>

	<xsl:template match="title">
		<h2 class="p-name" itemprop="headline">
			<xsl:apply-templates select="@*|node()"/>
		</h2>
	</xsl:template>

	<xsl:template match="pubdate">
		<p><xsl:call-template name="date">
				<xsl:with-param name="datetime" select="."/>
				<xsl:with-param name="class" select="'dt-published'"/>
		</xsl:call-template></p>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:apply-imports/>
	</xsl:template>

</xsl:stylesheet>
