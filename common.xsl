<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:date="http://exslt.org/dates-and-times"
	extension-element-prefixes="date"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template name="meta">
		<xsl:param name="description"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="description">
			<xsl:attribute name="content">
				<xsl:value-of select="$description"/>
			</xsl:attribute>
		</meta>
	</xsl:template>

	<xsl:template name="header">
		<xsl:param name="page"/>
		<header>
			<h1><img src="{$base}/perita.svg" width="64" height="90" alt="Perita" title="Perita"/></h1>
			<nav>
				<ul>
					<li>
						<xsl:if test="$page = 'games'">
							<xsl:attribute name="aria-current">
								<xsl:value-of select="'page'"/>
							</xsl:attribute>
						</xsl:if>
						<a rel="self" href="{$base}/index.html">
							<xsl:if test="$page = 'games'">
								<xsl:attribute name="rel">
									<xsl:value-of select="'self'"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:text>Games</xsl:text>
						</a>
					</li>
					<li>
						<xsl:if test="$page = 'blog' or $page = 'post'">
							<xsl:attribute name="aria-current">
								<xsl:value-of select="'page'"/>
							</xsl:attribute>
						</xsl:if>
						<a href="{$base}/blog/index.html">
							<xsl:if test="$page = 'blog'">
								<xsl:attribute name="rel">
									<xsl:value-of select="'self'"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="$page = 'post'">
								<xsl:attribute name="rel">
									<xsl:value-of select="'index'"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:text>Blog</xsl:text>
						</a>
					</li>
				</ul>
			</nav>
		</header>
	</xsl:template>

	<xsl:template name="footer">
		<footer>
			<p class="h-card">© 2020–2023 <a class="p-name p-org u-url" rel="me" href="https://peritasoft.com/">Perita</a> — <a class="u-email" href="mailto:info@peritasoft.com">info@peritasoft.com</a>.</p>
		</footer>
	</xsl:template>

	<xsl:template name="date">
		<xsl:param name="datetime"/>
		<xsl:param name="class"/>
		<time itemprop="datePublished" datetime="{$datetime}">
			<xsl:if test="$class != ''">
				<xsl:attribute name="class">
					<xsl:value-of select="$class"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="date:day-in-month($datetime)"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="date:month-name($datetime)"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="date:year($datetime)"/>
		</time>
	</xsl:template>

	<xsl:template name="storebadge">
		<xsl:param name="name"/>
		<xsl:param name="alt"/>
		<xsl:param name="defaultAlt"/>
		<img width="130" height="40" src="{$base}/store-badges/{$name}.png" srcset="{$base}/store-badges/{$name}.png 1x, {$base}/store-badges/{$name}@2x.png 2x">
			<xsl:attribute name="alt">
				<xsl:choose>
					<xsl:when test="$alt != ''"><xsl:value-of select="$alt"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$defaultAlt"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</img>
	</xsl:template>

	<xsl:template match="googleplay">
		<a href="https://play.google.com/store/apps/details?id={@appid}" style="display: inline-block">
			<xsl:call-template name="storebadge">
				<xsl:with-param name="name" select="'google'"/>
				<xsl:with-param name="alt" select="@alt"/>
				<xsl:with-param name="defaultAlt" select="'Get it on Google Play'"/>
			</xsl:call-template>
		</a>
	</xsl:template>

	<xsl:template match="appstore">
		<a href="{@href}" style="display: inline-block">
			<xsl:call-template name="storebadge">
				<xsl:with-param name="name" select="'apple'"/>
				<xsl:with-param name="alt" select="@alt"/>
				<xsl:with-param name="defaultAlt" select="'Download on the App Store'"/>
			</xsl:call-template>
		</a>
	</xsl:template>

	<xsl:template match="itchio">
		<a style="display: inline-block" href="https://perita.itch.io/{@slug}">
			<xsl:call-template name="storebadge">
				<xsl:with-param name="name" select="'itchio'"/>
				<xsl:with-param name="alt" select="@alt"/>
				<xsl:with-param name="defaultAlt" select="'Available on itch.io'"/>
			</xsl:call-template>
		</a>
	</xsl:template>

	<xsl:template match="productname">
		<xsl:variable name="element">
			<xsl:choose>
				<xsl:when test="@href != ''">a</xsl:when>
				<xsl:when test="@xml:lang != '' and @xml:lang != 'en'">i</xsl:when>
				<xsl:otherwise>cite</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:element name="{$element}">
			<xsl:attribute name="class">h-product p-name<xsl:if test="$element = 'i'"> foreignphrase</xsl:if></xsl:attribute>
			<xsl:apply-templates select="@*"/>
			<xsl:if test="@href != ''">
				<xsl:attribute name="href">
					<xsl:value-of select="@href"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="orgname">
		<xsl:variable name="element">
			<xsl:choose>
				<xsl:when test="@href != ''">a</xsl:when>
				<xsl:otherwise>span</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:element name="{$element}">
			<xsl:attribute name="class">h-card p-name p-org</xsl:attribute>
			<xsl:if test="@href != ''">
				<xsl:attribute name="href">
					<xsl:value-of select="@href"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="keycap|mousebutton">
		<kbd class="{local-name()}"><xsl:apply-templates /></kbd>
	</xsl:template>

	<xsl:template match="programlisting">
		<figure><pre><code><xsl:apply-templates /></code></pre></figure>
	</xsl:template>

	<xsl:template match="screen">
		<figure><pre><xsl:apply-templates /></pre></figure>
	</xsl:template>

	<xsl:template match="foreignphrase">
		<i class="foreignphrase">
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</i>
	</xsl:template>

	<xsl:template match="command">
		<kbd><xsl:apply-templates/></kbd>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
