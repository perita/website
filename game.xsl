<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="common.xsl" />

	<xsl:variable name="base" select="'..'"/>

	<xsl:output
		method="html"
		version="1.0"
		encoding="UTF-8"
		omit-xml-declaration="yes"
		indent="yes"
		doctype-system="about:legacy-compat"
		/>

	<xsl:template match="/">
		<html lang="en">
			<head>
				<xsl:call-template name="meta">
					<xsl:with-param name="description" select="game/info/abstract"/>
				</xsl:call-template>
				<title><xsl:apply-templates select="game/info/productname" mode="text"/> — Perita</title>
				<link rel="stylesheet" type="text/css" media="screen" href="../game.css" />
				<xsl:apply-templates select="game/info/style"/>
			</head>
			<body>
				<header>
					<p><a href="../index.html" rel="home">← Return to main page</a></p>
				</header>
				<article itemscope="itemscope" itemtype="http://schema.org/SoftwareApplication" class="h-product">
					<h1>
						<xsl:attribute name="itemprop">name</xsl:attribute>
						<xsl:attribute name="class">p-name</xsl:attribute>
						<xsl:apply-templates select="game/info/productname/node()"/>
					</h1>
					<xsl:apply-templates select="game/gameframe"/>
					<div itemprop="description" class="e-content">
						<xsl:apply-templates select="game/*[not(self::info) and not(self::gameframe)]"/>
						<xsl:apply-templates select="game/info/assets"/>
					</div>
					<xsl:apply-templates select="game/info"/>
					<xsl:apply-templates select="game/info/stores"/>
				</article>

				<xsl:call-template name="footer"/>
				<script src="../game.js"></script>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="assets">
		<section>
			<h2>Assets</h2>
			<xsl:apply-templates/>
		</section>
	</xsl:template>

	<xsl:template match="fonts|music|sounds|graphics">
		<section>
			<h3>
				<xsl:value-of select="concat(translate(substring(local-name(), 1, 1), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), substring(local-name(), 2))"/>
			</h3>
			<ul>
				<xsl:apply-templates/>
			</ul>
		</section>
	</xsl:template>

	<xsl:template match="asset">
		<li itemscope="itemscope" itemtype="https://schema.org/CreativeWork"><xsl:apply-templates/></li>
	</xsl:template>

	<xsl:template match="cite">
		<cite itemprop="name">
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</cite>
	</xsl:template>

	<xsl:template match="author">
		<span itemprop="author" itemscope="itemscope" itemtype="https://schema.org/Person" class="h-card">
			<span itemprop="name" class="p-name">
				<xsl:apply-templates />
			</span>
			<xsl:text> (</xsl:text>
			<a itemprop="url" class="u-url">
				<xsl:attribute name="href">
					<xsl:value-of select="@href"/>
				</xsl:attribute>
				<xsl:value-of select="substring-after(@href, '://')"/>
			</a>
			<xsl:text>)</xsl:text>
		</span>
	</xsl:template>

	<xsl:template match="license">
		<br/>
		Licensed under <xsl:apply-templates select="." mode="link"/>
	</xsl:template>

	<xsl:template match="license|codelicense|assetlicense" mode="link">
		<xsl:variable name="license-id" select="."/>
		<xsl:variable name="license-url">
			<xsl:choose>
				<xsl:when test="@href != ''">
					<xsl:value-of select="@href"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="document('spdx.xml')//license[@id = $license-id]/url"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$license-url != ''">
				<a>
					<xsl:attribute name="itemprop">license</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:value-of select="$license-url"/>
					</xsl:attribute>
					<xsl:apply-templates select="." mode="name"/>
				</a>
			</xsl:when>
			<xsl:otherwise><xsl:apply-templates select="." mode="name"/></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="license|codelicense|assetlicense" mode="name">
		<xsl:variable name="license-id" select="."/>
		<xsl:variable name="license-name" select="document('spdx.xml')//license[@id = $license-id]/name"/>
		<cite>
			<xsl:choose>
				<xsl:when test="$license-name != ''"><xsl:value-of select="$license-name"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="$license-id"/></xsl:otherwise>
			</xsl:choose>
		</cite>
	</xsl:template>

	<xsl:template match="info">
		<details>
			<summary>More information</summary>
			<dl>
				<dt>Type</dt>
				<dd itemprop="applicationCategory">Game</dd>
				<xsl:apply-templates mode="details"/>
			</dl>
		</details>
	</xsl:template>

	<xsl:template match="productname|abstract|assets|style|stores" mode="details">
	</xsl:template>

	<xsl:template match="pubdate" mode="details">
		<dt>Published</dt>
		<dd><xsl:call-template name="date">
				<xsl:with-param name="datetime" select="."/>
		</xsl:call-template></dd>
	</xsl:template>

	<xsl:template match="genre" mode="details">
		<dt>Genre</dt>
		<dd itemprop="applicationSubcategory"><xsl:apply-templates/></dd>
	</xsl:template>

	<xsl:template match="keywords" mode="details">
		<dt>Tags</dt>
		<dd><xsl:apply-templates/></dd>
	</xsl:template>

	<xsl:template name="platforms">
		<xsl:param name="platform"/>
		<xsl:if test="string-length($platform)">
			<xsl:if test="not($platform = .)">
				<xsl:text>, </xsl:text>
			</xsl:if>
			<span itemprop="operatingSystem">
				<xsl:value-of select="substring-before(concat($platform, ','), ',')"/>
			</span>
			<xsl:call-template name="platforms">
				<xsl:with-param name="platform" select="substring-after($platform, ',')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template match="platforms" mode="details">
		<dt>Platforms</dt>
		<dd><xsl:call-template name="platforms"><xsl:with-param name="platform" select="."/></xsl:call-template></dd>
	</xsl:template>

	<xsl:template match="programinglanguage" mode="details">
		<dt>Programing language</dt>
		<dd><xsl:apply-templates/></dd>
	</xsl:template>

	<xsl:template match="madewith" mode="details">
		<dt>Made with</dt>
		<dd><xsl:apply-templates/></dd>
	</xsl:template>

	<xsl:template match="coderepository" mode="details">
		<dt>Repository</dt>
		<dd>
			<a>
				<xsl:attribute name="href">
					<xsl:apply-templates mode="attribute"/>
				</xsl:attribute>
				<xsl:apply-templates/>
			</a>
		</dd>
	</xsl:template>

	<xsl:template match="codelicense" mode="details">
		<dt>Code license</dt>
		<dd><xsl:apply-templates select="." mode="link"/></dd>
	</xsl:template>

	<xsl:template match="assetlicense" mode="details">
		<dt>Asset license</dt>
		<dd><xsl:apply-templates select="." mode="link"/></dd>
	</xsl:template>

	<xsl:template match="gameframe">
		<div id="game-frame">
			<xsl:attribute name="style">
				<xsl:value-of select="concat('width:', @width, 'px;height:', @height, 'px;')"/>
			</xsl:attribute>
			<div id="iframe-placeholder">
				<button id="run-game">
					<svg role="img" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" width="24" class="svgicon icon-play" fill="none" height="24" version="1.1" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg>
					Run game
				</button>
			</div>
			<template id="iframe-template"><iframe allow="autoplay; fullscreen *" scrolling="no" allowfullscreen="true" frameborder="0">
					<xsl:attribute name="src">
						<xsl:value-of select="@src"/>
					</xsl:attribute>
			</iframe></template>
		</div>
	</xsl:template>

	<xsl:template match="stores">
		<section id="stores">
			<h2>Also available on</h2>
			<ul>
				<xsl:apply-templates/>
			</ul>
		</section>
	</xsl:template>

	<xsl:template match="appstore|googleplay|itchio">
		<li><xsl:apply-imports/></li>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:apply-imports/>
	</xsl:template>
</xsl:stylesheet>
